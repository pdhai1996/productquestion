<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 08:40
 */
namespace Magenest\ProductQuestion\Controller\Question;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\ResultFactory;

class Post extends Action{

    public function execute(){

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try
        {
            $data = $this->getRequest()->getPostValue();
            $question = $this->_objectManager->create('Magenest\ProductQuestion\Model\Question');
            $question->setData($data);
            $question->save();
            $this->messageManager->addSuccess(
                __('We have received your request and will get back to you with further details shortly.')
            );
        }

        catch (\Exception $e) {
            $resultRedirect->setPath('/');
            return $resultRedirect;
        }

        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}

