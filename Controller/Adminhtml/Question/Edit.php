<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 28/02/2017
 * Time: 16:22
 */
namespace Magenest\ProductQuestion\Controller\Adminhtml\Question;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class Edit extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('question_id');
        $model = $this->_objectManager->create('Magenest\ProductQuestion\Model\Question');
        // $quest = $this->_objectManager->get("Magenest\ProductQuestion\Model\Question");
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This mapping no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        // 4. Register model to use later in blocks
        $this->_coreRegistry->register('productquestion', $model);

        $resultPage = $this->_initAction();

        // TITLE
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? __('Product Question') : __('Question Setting'));
        return $resultPage;
    }

    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_ProductQuestion::productquestion')
            ->addBreadcrumb(__('Manage Question'), __('Manage Question'));
        return $resultPage;
    }
}