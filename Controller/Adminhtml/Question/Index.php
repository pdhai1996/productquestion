<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 25/02/2017
 * Time: 10:11
 */
namespace Magenest\ProductQuestion\Controller\Adminhtml\Question;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_ProductQuestion::index');
        $resultPage->addBreadcrumb(__('ProductQuestion'), __('ProductQuestion'));
        $resultPage->addBreadcrumb(__('Manage Question'), __('Manage Questions'));
        $resultPage->getConfig()->getTitle()->prepend(__('Questions'));
        return $resultPage;
    }
//    protected function _isAllowed()
//    {
//        return $this->_authorization->isAllowed('Magenest_SimpleModule::index');
//    }
}