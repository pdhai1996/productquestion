<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 28/02/2017
 * Time: 15:20
 */
namespace Magenest\ProductQuestion\Controller\Adminhtml\Question;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class MassDelete extends \Magento\Backend\App\Action {
    protected $row;
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magenest\ProductQuestion\Model\QuestionFactory $row
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->row=$row;
        parent::__construct($context);
    }

    public function execute()
    {
        $ids = $this->getRequest()->getParam('question_id');
        if (!is_array($ids)) {
            $this->messageManager->addNotice(__('Please select question(s).'));
        } else {
            try {
                $photo = $this->row->create();
                foreach ($ids as $id) {
                    $photo->load($id)->delete();
                }
                $this->messageManager->addSuccess(__('Total of %1 record(s) were deleted.'), count($ids));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

}