<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 08:26
 */
namespace Magenest\ProductQuestion\Model;
class Question extends \Magento\Framework\Model\AbstractModel {
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource =
        null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection =
        null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource,
            $resourceCollection, $data);
    }
    public function _construct() {
        $this->_init('Magenest\ProductQuestion\Model\ResourceModel\Question');
    }

    public function getDataCollection()
    {
        $collection = $this->getCollection()->addFieldToSelect('*');
        return $collection;
    }

}