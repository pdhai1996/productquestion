<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 14:08
 */
namespace Magenest\ProductQuestion\Model\ResourceModel;
class Answer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('magenest_product_answer', 'answer_id');
    }
}