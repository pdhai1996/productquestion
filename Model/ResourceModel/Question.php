<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 08:26
 */
namespace Magenest\ProductQuestion\Model\ResourceModel;
class Question extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('magenest_product_question', 'question_id');
    }
}