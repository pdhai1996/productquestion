<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 08:26
 */
namespace Magenest\ProductQuestion\Model\ResourceModel\Question;
/**
 * Question Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct() {
        $this->_init('Magenest\ProductQuestion\Model\Question',
            'Magenest\ProductQuestion\Model\ResourceModel\Question');
    }
}