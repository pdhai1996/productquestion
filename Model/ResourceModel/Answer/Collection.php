<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/02/2017
 * Time: 14:08
 */
namespace Magenest\ProductQuestion\Model\ResourceModel\Answer;
/**
 * Question Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct() {
        $this->_init('Magenest\ProductQuestion\Model\Answer',
            'Magenest\ProductQuestion\Model\ResourceModel\Answer');
    }
}