<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 13/02/2017
 * Time: 09:29
 */
namespace Magenest\ProductQuestion\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Db\Ddl\Table as Table;
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
            $installer = $setup;
            $installer->startSetup();
//Install new database table
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_product_question')
            )->addColumn(
                'question_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null, [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
                'Question Id'
            )->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null, [
                'nullable' => false,
                'unsigned' =>true
            ],
                'Product ID'
            )->addColumn(
                'question',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null, [
                'nullable' => false,
                'default' => ''
            ],
                'Question'
            )->addColumn(
                'author_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                 'nullable' => false,
                  'default' => ''
                ],
                'Author name'
            )->addColumn(
                'author_email',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Author Email'
            )->addColumn(
                'telephone_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                11,
                ['nullable' => false, 'default' => ''],
                'Telephone Number'
            )
                ->addColumn(
                'is_active',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                2,
                ['nullable' => false, 'default' => '0'],
                'Is Active'
            )->addColumn(
                'visible_in_fronted',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => '1'],
                'Visible In Frontend'
            )->addColumn(
                'total_answers',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => '0'],
                'Total  Answers'
            )->addColumn(
                'is_customer_subcribe',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                2, [
                'nullable' => false,
                'default' => '1',
            ],
                'Is Subcribe'
            )->addColumn(
                'create_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null, [
                'nullable' => false
            ],
                'Created At'
            )->setComment(
                'Product Question'
            );
            $installer->getConnection()->createTable($table);


// Create table product answer


        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_product_answer')
        )->addColumn(
            'answer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null, [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ],
            'Answer Id'
        )->addColumn(
            'question_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null, [
            'nullable' => false,
            'unsigned' =>true
        ],
            'Question Id'
        )->addColumn(
            'answer',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null, [
            'nullable' => false,
            'default' => ''
        ],
            'Answer'
        )->addColumn(
            'author_answer',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255,
            [
                'nullable' => false,
                'default' => ''
            ],
            'Author answer name'
        )->addColumn(
            'time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false],
            'Time'
        )->setComment(
            'Product Answer'
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}