<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 25/02/2017
 * Time: 11:26
 */
namespace Magenest\ProductQuestion\Block\Adminhtml;
class Question extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Magento_ProductQuestion';
        $this->_controller = 'adminhtml_question';
        parent::_construct();
    }
}