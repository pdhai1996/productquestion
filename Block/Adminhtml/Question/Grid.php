<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 25/02/2017
 * Time: 11:26
 */
namespace Magenest\ProductQuestion\Block\Adminhtml\Question;
use Magento\Backend\Block\Widget\Grid as WidgetGrid;
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magenest\ProductQuestion\Model\ResourceModel\Question\Collection
     */
    protected $_questionCollection;
    /**
     * @var \Magenest\ProductQuestion\Model\Status
     */
    protected $_status;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magenest\ProductQuestion\Model\ResourceModel\Question\Collection
    $questionCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magenest\ProductQuestion\Model\ResourceModel\Question\Collection
        $questionCollection,
        \Magenest\ProductQuestion\Model\Status $status,
        array $data = []
    ) {
        $this->_questionCollection = $questionCollection;
        $this->_status= $status;
        parent::__construct($context, $backendHelper, $data);
        $this->setEmptyText(__('No questions Found'));
    }
    /**
     * Initialize the subscription collection
     *
     * @return WidgetGrid
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->_questionCollection);
        return parent::_prepareCollection();
    }

/**
* Prepare grid columns
*
* @return $this
*/
    protected function _prepareColumns()
    {
        $this->addColumn(
            'question_id',
            [
                'header' => __('ID'),
                'index' => 'question_id',
            ]
        );
        $this->addColumn(
            'product_id',
            [
                'header' => __('Product ID'),
                'index' => 'product_id',
            ]
        );
        $this->addColumn(
            'question',
            [
                'header' => __('Question'),
                'index' => 'question',
            ]
        );
        $this->addColumn(
            'author_name',
            [
                'header' => __('Author name'),
                'index' => 'author_name',
            ]
        );
        $this->addColumn(
            'author_email',
            [
                'header' => __('Author Email'),
                'index' => 'author_email',
            ]
        );
        $this->addColumn(
            'telephone_number',
            [
                'header' => __('Telephone_number'),
                'index' => 'telephone_number',
            ]
        );
        $this->addColumn(
            'is_active',
            [
                'header' => __('Is Active'),
                'type' => 'options',
                'index' => 'is_active',
                'options' => $this->_status->getOptionArrayActive()
            ]
        );
        $this->addColumn(
            'visible_in_frontend',
            [
                'header' => __('Visible In Frontend'),
                'index' => 'visible_in_frontend',
                'type' => 'options',
                'options' => $this->_status->getOptionArray()
            ]
        );
        $this->addColumn(
            'total_answers',
            [
                'header' => __('Total Answers'),
                'index' => 'total_answers',
            ]
        );
        $this->addColumn(
            'is_customer_subcribe',
            [
                'header' => __('Is Customer Subcribe'),
                'type' => 'options',
                'index' => 'is_customer_subcribe',
                'options' => $this->_status->getOptionArray()
            ]
        );
        $this->addColumn(
            'created_at',
            [
                'header' => __('Created At'),
                'index' => 'created_at',
            ]
        );

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl(
            'productquestion/*/edit',
            ['question_id' => $row->getId()]
        );
    }
//
    protected function _prepareMassaction(){
        $this->setMassactionIdField('question_id');
        $this->getMassactionBlock()->setFormFieldName('question_id');
        $this->getMassactionBlock()->addItem('delete',array('label' => __('Delete'),'url' => $this->getUrl('*/*/massdelete'),'confirm' => 'Are you sure?'));
        $statuses = $this->_status->getOptionArrayActive();
        $this->getMassactionBlock()->addItem(
            'active',
            [
                'label' => __('Change active'),
                'url' => $this->getUrl('productquestion/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'active',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Is Active'),
                        'values' => $statuses
                    ]
                ]
            ]
        );
        return $this;
    }
}
