<?php
namespace Magenest\ProductQuestion\Block\Product\View;

use Magento\Catalog\Model\Product;

class Attributes extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magenest\ProductQuestion\Model\QuestionFactory
     */
    protected $_questionFactory;
    /**
     * @var \Magenest\ProductQuestion\Model\AnswerFactory
     */
    protected $_answerFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    /**
     * @var Product
     */
    protected $_product = null;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\ProductQuestion\Model\QuestionFactory $questionFactory,
        \Magenest\ProductQuestion\Model\AnswerFactory $answerFactory,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_answerFactory = $answerFactory;
        $this->_questionFactory = $questionFactory;
        $this->_coreRegistry = $registry;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }


    /**
     * @return string
     */
    public function getAction()
    {
        //redirect to ProductQuestion/Controller/Question/Index.php
        return $this->getUrl('productquestion/question/post');
    }

    protected function construct(){
        parent::_construct();
    }

    /**
     * Check Question Customer
     *
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if($this->isCustomerLoggedIn())
        {$data = $this->_customerSession->getCustomer();}
        else {$data='No customer loged in';}
        return $data;
    }

    public function getEmail(){
        $customer=$this->getCustomer();
        $email=$customer['email'];
        return $email;

    }
    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        
        return (boolean)$this->_customerSession->isLoggedIn();
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getQuestion(){
        $collection = $this->_questionFactory->create()->getCollection();
        return $collection;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getAnswer(){
        $answerCollection = $this->_answerFactory->create()->getCollection();
        return $answerCollection;
    }

    /**
     * Get Product Id
     *
     * @return int
     */
    public function getProductId(){
        $product = $this->getProduct();
        return $product->getId();
    }

    /**
     * @return Product|mixed
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }
}