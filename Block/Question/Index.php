<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 13/02/2017
 * Time: 08:44
 */
namespace Magenest\ProductQuestion\Block\Question;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magenest\ProductQuestion\Model\QuestionFactory
     */
    protected $_questionFactory;
    /**
     * @var \Magenest\ProductQuestion\Model\AnswerFactory
     */
//    protected $_answerFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    /**
     * @var \Magenest\ProductQuestion\Model\AnswerFactory
     */
    protected $_answerFactory;
//    protected $_coreRegistry = null;
    /**
     * @var Product
     */
    protected $_product = null;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\ProductQuestion\Model\QuestionFactory $questionFactory,
        \Magenest\ProductQuestion\Model\AnswerFactory $answerFactory,
        array $data = []
    ) {
        $this->_questionFactory = $questionFactory;
        $this->_customerSession = $customerSession;
        $this->_answerFactory = $answerFactory;
        parent::__construct($context, $data);
    }


    protected function construct(){
        parent::_construct();
    }

    /**
     * Check Question Customer
     *
     * @return \Magento\Customer\Model\Customer
     */

    public function getCustomer()
    {
        $data = $this->_customerSession->getCustomer();

        return $data;
    }

    /**
     * Get Product Id
     *
     * @return int
     */

    

    public function getQuestion(){
        $collection = $this->_questionFactory->create()->getCollection();
        return $collection;
    }

    public function getCustomerQuestion(){
       $customer=$this->getCustomer();
        $email=$customer['email'];

        $collection = $this->_questionFactory->create()
            ->getCollection()
            ->addFieldToFilter('author_email', $email)
            ->getData();
        return $collection;
    }

    public function getAnswers($question){
        $questionId= $question['question_id'];
        $collection = $this->_answerFactory->create()->getCollection()->addFieldToFilter('question_id',$questionId)->getData();
        return $collection;
    }
}
//    public function getLandingsUrl()
//    {
//        return $this->getUrl('helloworld');
//    }
//
//    public function getRedirectUrl()
//    {
//        return $this->getUrl('helloworld/index/redirect');
//    }
